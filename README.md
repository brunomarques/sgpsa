# Projeto Sistema de Gerenciamento de Peças e Serviços Automotivos - SGPSA 1.0

Tecnologias Utilizadas

- Java Server Faces
- Primefaces
- Maven
- Hibernate + JPA
- Mysql

--
Requisitos

- Java JDK 1.8
- Apache Tomcat 8

--
Instalação

Após o download, deve seguir os seguintes passos:

- Baixar o Tomcat na versão 8
- Adicionar o projeto ao servidor
- Editar o arquivo hibernate.cfg.xml (configuração de banco de dados) usuário: root, senha: sem senha
- Criar um banco de dados no mysql com o nome db_projeto_integrado
- Executar o projeto iniciando o servidor.